## Data Import of National Addresses (DINA) system

Ovo je repozitorijum i source code sa sajta https://dina.openstreetmap.rs.

Država Srbija je izbacila registre ulica, adresa i stambenih zajednica kao otvorene podatke. DINA platforma uvozi ove
podatke, analizira ih i pomaže maperima prilikom uvoza ovih podatak u OSM.

Trenutno postoje dve komponente:

### Adrese iz adresnog registra

Pogledajte u direktorijumu `ar/` gde ćete naći izvorni kod i dokumentaciju u [README.md](./ar/README.md) fajlu.

### Registar stambenih zajednica

Pogledajte u direktorijumu `sz/` gde ćete naći izvorni kod i dokumentaciju u [README.md](./sz/README.md) fajlu.